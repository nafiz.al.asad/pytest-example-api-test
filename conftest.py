import configparser
import os
import pytest


@pytest.fixture(scope='function')
def config():
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    _file = os.path.abspath(cur_dir) + "/" + 'config.ini'
    parser = configparser.ConfigParser()
    parser.read(_file)
    return parser


@pytest.fixture(scope='function')
def endpoint():
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    _file = os.path.abspath(cur_dir) + "/" + 'endpoints.ini'
    parser = configparser.ConfigParser()
    parser.read(_file)
    return parser
