import os
import configparser


class DataReader:
    def data(self):
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        _file = os.path.abspath(cur_dir)[:-6] + "\\" + 'data.ini'
        parser = configparser.ConfigParser()
        parser.read(_file)
        return parser
