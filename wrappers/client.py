import requests
import sys


class Client:
    def request(self, url, method, headers, body=''):
        method = str.lower(method)
        response = None
        try:
            if method == 'get':
                response = requests.get(url=url, headers=headers, data=body)
            elif method == 'post':
                response = requests.post(url=url, headers=headers, data=body)
            elif method == 'put':
                response = requests.put(url=url, headers=headers, data=body)
            elif method == 'delete':
                response = requests.delete(url=url, headers=headers, data=body)
        except Exception as e:
            #raise e
            sys.exit("Exiting with error", e)
        finally:
            return response
