import requests
from wrappers.client import Client
import json


class DUMMY(Client):
    def __init__(self, config, endpoint):
        self.baseurl = config['DUMMY']['baseurl']
        self.endpoint = endpoint['DUMMY']['employee']

    def get_employee(self, employee_id=None):
        _url = self.baseurl + self.endpoint
        if employee_id is None:
            response = self.request(url=_url, method='get', headers='')
        else:
            _url = self.baseurl + self.endpoint + "{employee_id}".format(employee_id=employee_id)
            response = self.request(url=_url, method='get', headers='')
        return response
