 **Setup**
-----------
1. Install Python 3.7.X
2. Clone the project
3. Create a virtual environment inside the project root directory
4. Activate virtual environment
5. Run following command in project root directory
`pip install -r requirements.txt`
6. Once the required packages are installed you are ready to write your tests

**Writing Tests**
-----------
1. Create a new branch from master branch that you have just cloned
2. Edit the template in _wrappers/sample_wrapper.py_ file to create your API wrapper classes
3. Edit the template in _tests/test_sample.py_ file to write tests using your API wrapper classes

**Running Tests**
----------
To run the tests run the command `py.test tests --html=report.html --csv=report.csv --junit-xml=report.xml --capture=no`
this will run the test modules in tests folder and generate designated reports and logs.

**Note**
-----
If you are using PyCharm IDE then, after cloning the project open the project directory in PyCharm and create a
virtual environment from PyCharm's project settings. After that if you open terminal in PyCharm it will automatically 
activate the virtual environment.

For more about Python virtual environment see here - https://docs.python.org/3/tutorial/venv.html