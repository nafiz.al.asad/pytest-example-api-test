from wrappers.sample_wrapper import DUMMY
from utils.data_reader import DataReader
import json
import pytest

class TestUserLogin:
    data_reader = DataReader()
    data = data_reader.data()
    employee_id = data['DUMMY']['employee.id']

    @pytest.mark.parametrize('employee_id',[employee_id])
    def test_get_employee(self, config, endpoint, employee_id):
        dummy = DUMMY(config, endpoint)
        response = dummy.get_employee()
        assert response.status_code == 200, "Status code is not 200"


